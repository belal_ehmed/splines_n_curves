﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointFollow : MonoBehaviour {

    public Transform[] targets;
    int current_target = 0;

    void Update() {

        if(Input.GetKeyDown(KeyCode.A))
        {
            current_target++;
            if(current_target>targets.Length-1)
                current_target=0;
        }

        transform.LookAt(targets[current_target].transform);
    }

}
