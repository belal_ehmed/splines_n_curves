﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveCurves : MonoBehaviour {

    //Public 
    public GameObject target;
    public GameObject handle;
    public Vector3 p0;
    public Vector3 p1;  

    //Private
    private LineRenderer link;
    private int inputCount = 0;
    private Vector3[] points = new Vector3[3];
    private Vector3 anchorPos;


    private void Start() {
        link = gameObject.GetComponent<LineRenderer>();

        points[0] = p0;
        points[1] = p1;

        //initialAnchorPos = handle.transform.position;

        DrawLink(points);
    }

    private void Update() {
       
        /*
        if (Input.GetMouseButtonDown(0)) 
        {
            Vector3 mousePos = Input.mousePosition;
            
            GameObject tempObj = Instantiate(target.gameObject, Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 10.0f)), target.transform.rotation);

            points[inputCount] = tempObj.gameObject.transform.position;
            inputCount++;
            if (inputCount >= 2) {
                inputCount = 0;
                DrawLink(points);
            }       
        }
         */
    }

     void DrawLink(Vector3[] endPoints)
    {
        if (endPoints.Length < 2) {
            Debug.Log("Not enough points to draw link");
            return;
        }

        PlaceHandle(endPoints);

        link.positionCount = points.Length;
        link.SetPositions(points);
    }

    private void PlaceHandle(Vector3[] endPoints) 
    {
        Vector3 tempVec = endPoints[1];

        Vector3 midPt = CalculateMidPoint(endPoints[0],endPoints[1]);

        //points[1] = midPt;
        handle.transform.position = midPt;
        anchorPos = handle.transform.position;

        points[1] = midPt;
        points[2] = tempVec;
    }

    private Vector3 CalculateMidPoint(Vector3 p0, Vector3 p1)
    {
        Vector3 midpoint = new Vector3((p0.x + p1.x) / 2, (p0.y + p1.y) / 2, (p0.z + p1.z) / 2);
        return midpoint;
    }

    public void DrawAnchor(float deltaDrag) 
    {
        link.positionCount = 0;
        link.positionCount = points.Length;

        anchorPos = Vector3.zero;

        if (deltaDrag < 0) {
            anchorPos.x = p0.x;
            anchorPos.y = p1.y;
        }
        else if (deltaDrag > 0) {
            anchorPos.x = p1.x;
            anchorPos.y = p0.y;
        }

        anchorPos.z = points[0].z;

        handle.transform.position = anchorPos;

        points[0] = p0;
        points[1] = anchorPos;
        points[2] = p1;

        link.SetPositions(points);
    }

    public void ResetAnchor() 
    {
        handle.transform.position = anchorPos;
    }

}
