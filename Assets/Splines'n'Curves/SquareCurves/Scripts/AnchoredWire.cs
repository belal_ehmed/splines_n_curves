﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AnchoredWire : MonoBehaviour,IPointerDownHandler  {

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!GameManager.instance.isLatched)
        {
            Debug.Log("latched");
            GameManager.instance.isLatched = true;
            GameManager.instance.StartLink(transform.position);
        }
        else if (GameManager.instance.isLatched)
        {
            Debug.Log("Unlatched");
            GameManager.instance.isLatched = false;
            GameManager.instance.EndLink(transform.position);
        }
    }
}
