﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CurveHandle : MonoBehaviour
{
    public float thresholdX;
    public float thresholdY;
    public float deltaDragX;
    public float deltaDragY;

    private float initialX;
    private float initialY;
  
    public void OnMouseDown() 
    {
        initialX = transform.position.x;
        initialY = transform.position.y;
    }

    public void OnMouseDrag() 
    {
        Vector3 mousePos = Input.mousePosition;

        transform.position = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 10.0f));

        deltaDragX = transform.position.x - initialX;
        deltaDragY = transform.position.y - initialY;
        
        if (thresholdY<Mathf.Abs(deltaDragY))
        {
            //transform.position = new Vector3(-3.0f,transform.position.y, transform.position.z);

            InteractiveCurves curve = (InteractiveCurves)FindObjectOfType<InteractiveCurves>();
            curve.DrawAnchor(deltaDragY);
        }
        /*
        else if(thresholdX<Mathf.Abs(deltaDragX))
        {
            InteractiveCurves curve = (InteractiveCurves)FindObjectOfType<InteractiveCurves>();
            curve.ResetAnchor();
        }
         */
    }
    
    public void OnMouseUp() {
        if (deltaDragX != 0 || deltaDragY != 0) {
            InteractiveCurves curve = (InteractiveCurves)FindObjectOfType<InteractiveCurves>();
            curve.ResetAnchor();
        }
    }    
}
