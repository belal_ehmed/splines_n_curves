﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchoredCurve : MonoBehaviour {

    //Public
    public GameObject p0, p1;
    public Vector3 midpoint;

    //Private
    LineRenderer curve;
    Vector3[] points;
    
    void Start() 
    {
        curve = GetComponent<LineRenderer>();
        curve.positionCount = 3;
        

        midpoint = MathOps.instance.CalculateMidPoint(p0.transform.position,p1.transform.position);

        points = new Vector3[3];

        points[0] = p0.transform.position;
        points[1] = midpoint;
        points[2] = p1.transform.position;

        curve.SetPositions(points);
    }

    void Update() {
        Vector3 inputPos = Input.mousePosition;
        //inputPos.z = p0.transform.position.z;
        Vector3 newPos = Camera.main.ScreenToWorldPoint(inputPos);
        newPos.z = p0.transform.position.z;
        midpoint = newPos;
        points[0] = p0.transform.position;
        points[1] = inputPos;
        points[2] = p1.transform.position;

        Debug.Log(""+newPos);

        curve.SetPositions(points);
    }
}
