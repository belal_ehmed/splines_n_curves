﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region SINGLETON
    private static GameManager _instance = null;
    public static GameManager instance {
        get {
            if (_instance == null)
                _instance = FindObjectOfType<GameManager>();
            return _instance;
        }
    }
    #endregion

    #region VARS
    //Public
    public bool isLatched = false;
    public GameObject anchorUI;

    //Private
    private LineRenderer lineVFX;
    private Vector3[] poses;
    private Vector3[] anchorUIpos;
    #endregion

    #region INIT
    private void Start() 
    {
        lineVFX = GetComponent<LineRenderer>();
        poses = new Vector3[2];
        anchorUIpos = new Vector3[2];
    }
    #endregion

    #region UPDATE
    private void Update() 
    {
        if(isLatched){
            RenderLink();
        }
    }
    #endregion
    
    public void StartLink(Vector3 position) 
    {
        anchorUIpos[0] = position;

        GameObject linkObj = new GameObject("link");
        linkObj.transform.SetParent(transform);

        lineVFX = linkObj.AddComponent(typeof(LineRenderer)) as LineRenderer;
        position = Camera.main.ScreenToWorldPoint(new Vector3(position.x, position.y, 10.0f));

        lineVFX.positionCount = 2;
        
        poses[0] = position;
        poses[1] = new Vector3(-5, 0, 0);
        lineVFX.SetPositions(poses);
    }

    public void EndLink(Vector3 position) 
    {
        anchorUIpos[1] = position;
        
        Debug.Log("End link");
        position = Camera.main.ScreenToWorldPoint(new Vector3(position.x,position.y,10.0f));
        poses[1] = position;

        //lineVFX.SetPosition(1, position);
        Vector3[] newPoses = new Vector3[3];

        newPoses[0] = poses[0];
        newPoses[2] = poses[1];
        newPoses[1] = MathOps.instance.CalculateMidPoint(poses[0], poses[1]);

        lineVFX.positionCount = 3;
        lineVFX.SetPositions(newPoses);

        PlaceAnchorUI();
    }

    private void PlaceAnchorUI() {
        anchorUI.transform.position = MathOps.instance.CalculateMidPoint(anchorUIpos[0], anchorUIpos[1]);
    }

    public void AdjustAnchor(Vector3 position) {
        Vector3 newPos = Camera.main.ScreenToWorldPoint(new Vector3(position.x, position.y, 10.0f));
        lineVFX.SetPosition(1, newPos);
    }
    
    private void RenderLink()
    {
        Debug.Log("Rendering link");
        Vector3 mousePos = Input.mousePosition;
        lineVFX.SetPosition(1, Camera.main.ScreenToWorldPoint(new Vector3( mousePos.x, mousePos.y, 10.0f)));
    }
}
