﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareCurve : MonoBehaviour {

    //Public
    public Transform p0, p1;
    public LineRenderer lineRenderer;

    //Private
    private Vector3[] numPoints = new Vector3[30];
    private int btnCount;

    private void Update() {
        ProcessInput();
        DrawSquareCurve();
    }

    private void ProcessInput() {
        if(Input.GetMouseButtonDown(0)){
            btnCount++;
            
            if(btnCount>=2)
            {
                Debug.Log("Bypass");
                btnCount = 0;
                return;
            }


            Debug.Log("No Bypass");
        }
        Vector2 screenPos = Input.mousePosition;
        p1.position = Camera.main.ScreenToWorldPoint(new Vector3(screenPos.x, screenPos.y, 10f));
    }

    private void DrawSquareCurve() 
    {
        Vector3 midpoint = CalculateMidPoint(p0.position, p1.position);
        numPoints[0] = p0.position;
        numPoints[1] = new Vector3(midpoint.x, p0.position.y, p0.position.z);
        numPoints[2] = new Vector3(midpoint.x, p1.position.y, p1.position.z);
        numPoints[3] = p1.position;

        lineRenderer.positionCount = 4;
        lineRenderer.SetPositions(numPoints);
    }

    private Vector3 CalculateMidPoint(Vector3 p1, Vector3 p2)
    {
        Vector3 midpoint = new Vector3((p1.x + p2.x) / 2, (p1.y + p2.y) / 2, (p1.z + p2.z) / 2);
        return midpoint;
    }
}
