﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class curve_test : MonoBehaviour {

    //Public
    public Transform point1, point2;
    public GameObject target;

    //Private
    private int numPoints = 30;
    private Vector3[] positions = new Vector3[30];

    private void Start() {
        DrawCurve();
    }


    private void Update() {
        DrawCurve();
    }

    private void DrawCurve() {
        Vector3 mid = CalculateMidPoint(point1.position,point2.position);
        target.transform.position = mid;
    }

    private void CalculateCurve() { 
        
    }

    private Vector3 CalculateMidPoint(Vector3 p1, Vector3 p2) {
        Vector3 midpoint = new Vector3((p1.x+p2.x)/2,(p1.y+p2.y)/2,(p1.z+p2.z)/2);
        return midpoint;
    }
}
