﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    
public class Bezier : MonoBehaviour {

    //Public 
    public Transform point0, point1, point2,point3;

    //Protected
    
    //Private
    private LineRenderer lineRenderer;
    private int numPoints = 30;
    private Vector3[] positions = new Vector3[30];


    void Start() {
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.positionCount = numPoints;
        //DrawLinearCurve();
    }

    private void Update() {
        //DrawLinearCurve();
        //DrawQuadraticCurve();
        DrawCubicCurve();
    }

    
    //CURVE DRAWING
    private void DrawLinearCurve() 
    {
        //Calculate "t"
        for (int i = 1; i <= numPoints; i++) 
        {
            float t = i / (float)numPoints;
            positions[i - 1] = CalculateLinearBezierCurve(t, point0.position, point1.position);
        }
        lineRenderer.SetPositions(positions);
    }

    private void DrawQuadraticCurve()
    {
        //Calculate "t"
        for (int i = 1; i <= numPoints; i++)
        {
            float t = i / (float)numPoints;
            positions[i - 1] = CalculateQuadraticBezierCurve(t, point0.position, point1.position,point2.position);
        }
        lineRenderer.SetPositions(positions);
    }


    private void DrawCubicCurve() {
        for (int i = 1; i <= numPoints; i++) {
            float t = i / (float)numPoints;
            positions[i - 1] = CalculateCubicBezierCurve(t, point0.position, point1.position, point2.position, point3.position);
        }
        lineRenderer.SetPositions(positions);
    }
    
    //END



    //CURVE CALS
    private Vector3 CalculateLinearBezierCurve(float t, Vector3 p0, Vector3 p1)
    {
        //P=(1-t)P1+tP0 or P=P1+t(P1-P0)
        return p0 + t * (p1 - p0);
    }

    /*
    private Vector3 CalculateQuadraticBizierCurve(float t, Vector3 p0, Vector3 p1, Vector3 p2) {
        
    }*/

    private Vector3 CalculateQuadraticBezierCurve(float t, Vector3 p0, Vector3 p1, Vector3 p2)
    {
        // (1–t)2P0 + 2(1–t)tP1 + t2P2
        //  u          u          tt
        //  uu*p0   + 2*u*t*p1  + tt*p2
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;


        Vector3 p = uu * p0;
        p += 2 * u * t * p1;
        p += tt * p2;

        return p;
        //throw new System.NotImplementedException();
    }

    private Vector3 CalculateCubicBezierCurve(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3) {
        //p[x,y]= (1–t)3P0    +   3(1–t)2tP1  + 3(1–t)t2P2    +   t3P3
        //          u                u             u
        //        uuu*p0      +   3*uu*t*p1  + 3*u*tt*p2    +   ttt*p3

        float u = 1 - t;
        float uu = u * u;
        float uuu = uu * u;
        float tt = t * t;
        float ttt = tt*t;

        Vector3 p = uuu * p0;
        p += 3 * uu * t * p1;
        p += 3 * u * tt * p2;
        p += ttt * p3;

        return p;
    }
    //END
}