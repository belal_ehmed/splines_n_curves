﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LinksMgr : MonoBehaviour
{
    #region Singleton
    private static LinksMgr _instance = null;
    public static LinksMgr instance 
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<LinksMgr>();
            }
            return _instance;
        }
    }
    #endregion

    public int linkCount=0;
    private List<GameObject> links;
    public float linkStartWidth, linkEndWidth;
    public Color linkStartColor, linkEndColor;

    void Start() {
    }

    #region LinkModel
    //Model - Link Data
    public void AddLink(List<Vector3> points) 
    { 
        linkCount++;
        GameObject linkObj = new GameObject("Link " + linkCount);
        linkObj.AddComponent<LineRenderer>();
        linkObj.transform.SetParent(this.transform);

        RenderLink(linkObj, points);

        Debug.Log("OBJ " + linkObj.name);
    }
    #endregion
    
    #region LinkView
    //View - Link Render
    public void RenderLink(GameObject obj, List<Vector3> points)
    {
        LineRenderer lineRendrer = obj.GetComponent<LineRenderer>();
        lineRendrer.SetWidth(linkStartWidth, linkEndWidth);
        lineRendrer.SetColors(linkStartColor,linkStartColor);
        
        lineRendrer.SetVertexCount(points.Count);
        for (int i = 0; i < points.Count; i++) {
            lineRendrer.SetPosition(i, points[i]);
        }
    }
    #endregion
}
