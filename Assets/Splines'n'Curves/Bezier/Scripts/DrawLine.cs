﻿//Controller - Links controller

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrawLine : MonoBehaviour {

	private List<Vector3> points;
	LineRenderer lineRendrer;

	public List<LineRenderer> links;

	bool io=false;

	public enum Mode{
		line,
		curve,
	}
	Mode mode;

	void Start(){
		mode = Mode.line;
		points = new List<Vector3> ();
		links = new List<LineRenderer> ();
		lineRendrer = GetComponent<LineRenderer> ();
		//lineRendrer.SetVertexCount (5);
	}
		
	public void DrawLink(){
		ProcessInput ();
		//Render();
	}

	private void ProcessInput()
    {
		if (Input.GetMouseButtonDown(0)) 
        {
			Vector2 screenPos = Input.mousePosition;
			Vector3 worldPos = Camera.main.ScreenToWorldPoint (new Vector3 (screenPos.x, screenPos.y, 4f));

			if (points.Count < 2)
				points.Add (worldPos);

            if (points.Count == 2) {

                LinksMgr.instance.AddLink(points);
                points.Clear();
            }
			Debug.Log ("points: "+points.Count);
		}
	}

	public void Render()
    {
		if (mode == Mode.line) {
			RenderLine (points);
		}
	}

	void RenderLine(List<Vector3> drawingPoints)
    {
		lineRendrer.SetVertexCount (drawingPoints.Count);
        

		for (int i = 0; i < drawingPoints.Count; i++) 
		{
			lineRendrer.SetPosition (i, drawingPoints [i]);
		}

		if (points.Count == 2) {
			links.Add (lineRendrer);
			points.Clear ();
		}
	}

	void RenderLinks(){
		
	}

	public static void ShowMessage(){

		Debug.Log("MSG");
	}
}

