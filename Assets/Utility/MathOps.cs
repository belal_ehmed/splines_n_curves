﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathOps : MonoBehaviour {

    #region SINGLETON
    private static MathOps _instance = null;
    public static MathOps instance {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<MathOps>();
            return _instance;
        }
    }
    #endregion

    public Vector3 CalculateMidPoint(Vector3 p1, Vector3 p2)
    {
        Vector3 midpoint = new Vector3((p1.x + p2.x) / 2, (p1.y + p2.y) / 2, (p1.z + p2.z) / 2);
        return midpoint;
    }

}
