﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveCube : MonoBehaviour {

    public GameObject target;

    void Update() 
    {
        Vector3 input = Input.mousePosition;
        target.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(input.x, input.y, 10.0f));
    }
	
}
